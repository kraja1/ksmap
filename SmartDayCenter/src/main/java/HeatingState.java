import helpers.*;

import java.util.Random;

public class HeatingState {
    private double CurTemp = Constant.DEAFULT_HEATING_CURTEMP;
    private double SetTemp = Constant.DEFAULT_HEATING_SETTEMP;
    private boolean Heating = Constant.DEFAULT_HEATING_ON;
    private boolean HeatingAuto = Constant.DEFAULT_HEATING_AUTO;

    public HeatingState(double curTemp, double setTemp, boolean heating, boolean heatingAuto) {
        CurTemp = curTemp;
        SetTemp = setTemp;
        Heating = heating;
        HeatingAuto = heatingAuto;
    }

    public HeatingState() {}

    public HeatingState(HeatingState heating) {
        CurTemp = heating.getCurTemp();
        SetTemp = heating.getSetTemp();
        Heating = heating.isHeating();
        HeatingAuto = heating.isHeatingAuto();
    }

    public double getCurTemp() {
        return CurTemp;
    }

    public void setCurTemp(double curTemp) {
        CurTemp = curTemp;
    }

    public double getSetTemp() {
        return SetTemp;
    }

    public void setSetTemp(double setTemp) {
        SetTemp = setTemp;
    }

    public boolean isHeating() {
        return Heating;
    }

    public void setHeating(boolean heating) {
        Heating = heating;
    }

    public boolean isHeatingAuto() {
        return HeatingAuto;
    }

    public void setHeatingAuto(boolean heatingAuto) {
        HeatingAuto = heatingAuto;
    }

    /* Change endpoints and variables according to data from App */
    public void changeState(HeatingState change) {
        //Logger.write("Change heating state:", null);
        Logger.write("> Old: " + "Heating: " + this.isHeating() +
                ", CurTemp: " + this.getCurTemp() +
                ", SetTemp: " + this.getSetTemp() +
                ", HeatingAuto: " + this.isHeatingAuto(), null);

        Logger.write("> New: " + "Heating: " + change.isHeating() +
                ", CurTemp: " + change.getCurTemp() +
                ", SetTemp: " + change.getSetTemp() +
                ", HeatingAuto: " + change.isHeatingAuto(), null);

        //actual change
        this.toggleHeating(change.isHeating());
        this.changeTemperature(change.getSetTemp());
        this.HeatingAuto = change.isHeatingAuto();
        
        //Logger.write("**** Simulated POST Request for heating ****", null); //move to toggle and changetemp
    }
    /* Update info about endpoints on server */
    public void updateState() {
        Logger.write("Endpoints state check: heating:", null);
        Logger.write("**** Simulated GET request for heating ****", null);

        /* Simulated changes to temperature based on heating settings
        *  Comment out for real use */
        Random rand = new Random();
        if(rand.nextDouble() <= 0.75){
            if(this.CurTemp < this.SetTemp && this.Heating){
                this.CurTemp += 0.3;
                this.CurTemp = Math.round(this.CurTemp*10)/10.0;
            }

        }
        if(rand.nextDouble() <= 0.55){
            if(this.CurTemp > this.SetTemp){
                this.CurTemp -= 0.2;
                this.CurTemp = Math.round(this.CurTemp*10)/10.0;
            }
        }
        /* *** *** */

        Logger.write("> Received state: " + "Heating: " + this.isHeating() +
                ", CurTemp: " + this.getCurTemp() +
                ", SetTemp: " + this.getSetTemp() +
                ", HeatingAuto: " + this.isHeatingAuto(), null);

    }

    /* Partial modification methods */
    public void toggleHeating(boolean ht) {
        //Logger.write("Toggle Heating: Original: " + this.isHeating() + " New: " + ht, null);
        if(this.Heating != ht) {
            Logger.write("**** Simulated POST Request for heating: Heating " + ht + " ****", null);
        }
        this.Heating = ht;

    }
    public void changeTemperature(double temp) {
        //Logger.write("Change set temperature: Original: " + this.getSetTemp() + " New: " + temp, null);
        if(this.SetTemp != temp) {
            Logger.write("**** Simulated POST Request for heating: Set temperature " + temp + " ****", null);
        }
        this.SetTemp = temp;
    }
}
