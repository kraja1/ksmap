import helpers.*;

public class LightState {
    private boolean Light = Constant.DEFAULT_LIGHT_ON;
    private int Brightness = Constant.DEFAULT_LIGHT_BRIGHTNESS;
    private boolean AutoLight = Constant.DEFAULT_LIGHT_AUTO;
    private boolean WindowOpen = Constant.DEFAULT_WINDOW_OPEN;
    private boolean ShadeOpen = Constant.DEFAULT_SHADE_OPEN;
    private boolean WindowAuto = Constant.DEFAULT_WINDOW_AUTO;
    private boolean ShadeAuto = Constant.DEFAULT_SHADE_AUTO;

    public LightState(boolean light, int brightness, boolean autoLight, boolean windowOpen, boolean shadeOpen, boolean windowAuto, boolean shadeAuto) {
        Light = light;
        Brightness = brightness;
        AutoLight = autoLight;
        WindowOpen = windowOpen;
        ShadeOpen = shadeOpen;
        WindowAuto = windowAuto;
        ShadeAuto = shadeAuto;
    }

    public LightState() {}

    public LightState(LightState light) {
        Light = light.getLight();
        Brightness = light.getBrightness();
        AutoLight = light.getAutoLight();
        WindowOpen = light.getWindowOpen();
        ShadeOpen = light.getShadeOpen();
        WindowAuto = light.getWindowAuto();
        ShadeAuto = light.getShadeAuto();
    }

    public boolean getLight() {
        return Light;
    }

    public void setLight(Boolean light) {
        Light = light;
    }

    public int getBrightness() {
        return Brightness;
    }

    public void setBrightness(Integer brightness) {
        Brightness = brightness;
    }

    public boolean getAutoLight() {
        return AutoLight;
    }

    public void setAutoLight(Boolean autoLight) {
        AutoLight = autoLight;
    }

    public boolean getWindowOpen() {
        return WindowOpen;
    }

    public void setWindowOpen(Boolean windowOpen) {
        WindowOpen = windowOpen;
    }

    public boolean getShadeOpen() {
        return ShadeOpen;
    }

    public void setShadeOpen(Boolean shadeOpen) {
        ShadeOpen = shadeOpen;
    }

    public boolean getWindowAuto() {
        return WindowAuto;
    }

    public void setWindowAuto(Boolean windowAuto) {
        WindowAuto = windowAuto;
    }

    public boolean getShadeAuto() {
        return ShadeAuto;
    }

    public void setShadeAuto(Boolean shadeAuto) {
        ShadeAuto = shadeAuto;
    }

    /* Change endpoints settings and variables according to data from App */
    public void changeState(LightState change) {
        //Logger.write("Change light state:", null);
        Logger.write("> Old: " + "Light: " + this.getLight() +
                ", Brightness: " + this.getBrightness() +
                ", AutoLight: " + this.getAutoLight() +
                ", WindowOpen: " + this.getWindowOpen() +
                ", ShadeOpen: " + this.getShadeOpen() +
                ", WindowAuto: " + this.getWindowAuto() +
                ", ShadeAuto: " + this.getShadeAuto(), null);

        Logger.write("> New: " + "Light: " + change.getLight() +
                ", Brightness: " + change.getBrightness() +
                ", AutoLight: " + change.getAutoLight() +
                ", WindowOpen: " + change.getWindowOpen() +
                ", ShadeOpen: " + change.getShadeOpen() +
                ", WindowAuto: " + change.getWindowAuto() +
                ", ShadeAuto: " + change.getShadeAuto(), null);
        //actual change
        this.switchLight(change.getLight());
        this.changeBrightnessAbs(change.getBrightness());
        this.AutoLight = change.getAutoLight();
        if(change.getWindowOpen()) {
            this.openWindow();
        }
        else this.closeWindow();
        if(change.getShadeOpen()) {
            this.openShade();
        }
        else this.closeShade();
        this.WindowAuto = change.getWindowAuto();
        this.ShadeAuto = change.getShadeAuto();


    }

    /* Update info about endpoints on server */
    public void updateState() {
        Logger.write("Endpoints state check: light:", null);
        Logger.write("**** Simulated GET request for Light ****", null);
        /* Here would be request for actual smart light endpoint */

        /*Simulated received light state*/
        Logger.write("> Received: " + "Light: " + this.getLight() +
                ", Brightness: " + this.getBrightness() +
                ", AutoLight: " + this.getAutoLight() +
                ", WindowOpen: " + this.getWindowOpen() +
                ", ShadeOpen: " + this.getShadeOpen() +
                ", WindowAuto: " + this.getWindowAuto() +
                ", ShadeAuto: " + this.getShadeAuto(), null);
    }

    /* Partial modification methods */
    public void switchLight(boolean light) {
        //Logger.write("Toggle Lights: Original: " + this.getLight() + " New: " + light, null);
        if(this.Light != light) {
            Logger.write("**** Simulated POST Request for Light: Light " + light + " ****", null);
        }
        this.Light = light;
    }
    public void changeBrightnessAbs(int br) {
        //Logger.write("Change brightness: Original: " + this.getBrightness() + " New: " + br, null);
        if(this.Brightness != br && br >= 0 && br <= Constant.MAX_LIGHT_BRIGHTNESS) {
            Logger.write("**** Simulated POST Request for Light: Brightness " + br + " ****", null);
            this.Brightness = br;
        }
    }
    public void changeBrightnessRel(int change) {
        //Logger.write("Change brightness: Original: " + this.getBrightness() + " Offset: " + change, null);
        if(change != 0 && (this.Brightness + change) <= Constant.MAX_LIGHT_BRIGHTNESS && (this.Brightness + change) >= 0) {
            Logger.write("**** Simulated POST Request for Light: Brightness shift + " + change + " ****", null);
            this.Brightness = this.Brightness + change;
        }
    }
    public void openWindow() {
        //Logger.write("Open windows, originally: " + this.getWindowOpen(), null);
        if(!this.WindowOpen) {
            Logger.write("**** Simulated POST Request for Light - window: Open windows ****", null);
        }
        this.WindowOpen = true;
    }
    public void closeWindow() {
        //Logger.write("Close windows, originally: " + this.getWindowOpen(), null);
        if(this.WindowOpen) {
            Logger.write("**** Simulated POST Request for Light - window: Close windows ****", null);
        }
        this.WindowOpen = false;
    }
    public void openShade() {
        //Logger.write("Open shades, originally: " + this.getShadeOpen(), null);
        if(!this.ShadeOpen) {
            Logger.write("**** Simulated POST Request for Light - shade: Open shades ****", null);
        }
        this.ShadeOpen = true;
    }
    public void closeShade() {
        //Logger.write("Close shades, originally: " + this.getShadeOpen(), null);
        if(this.ShadeOpen) {
            Logger.write("**** Simulated POST Request for Light - window: Close shades ****", null);
        }
        this.ShadeOpen = false;
    }
}
