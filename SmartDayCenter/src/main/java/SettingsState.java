import java.util.Calendar;
import java.util.Date;
import helpers.*;

public class SettingsState {
    private String TimeUp = Constant.DEFAULT_SETTINGS_WAKE;
    private String TimeSleep = Constant.DEFAULT_SETTINGS_SLEEP;
    private double DayTemp = Constant.DEFAULT_SETTINGS_DAYTEMP;
    private double NightTemp = Constant.DEFAULT_SETTINGS_NIGHTTEMP;
    private String City = Constant.DEFAULT_SETTINGS_CITY;
    private String Server = Constant.DEFAULT_SETTINGS_SERVER;


    public SettingsState(String timeUp, String timeSleep, double dayTemp, double nightTemp, String city, String server) {
        TimeUp = timeUp;
        TimeSleep = timeSleep;
        DayTemp = dayTemp;
        NightTemp = nightTemp;
        City = city;
        Server = server;
    }

    public SettingsState() {
    }

    public SettingsState(SettingsState settings) {
        TimeUp = settings.getTimeUp();
        TimeSleep = settings.getTimeSleep();
        DayTemp = settings.getDayTemp();
        NightTemp = settings.getNightTemp();
        City = settings.getCity();
        Server = settings.getServer();
    }

    public String getTimeUp() {
        return TimeUp;
    }

    public void setTimeUp(String timeUp) {
        TimeUp = timeUp;
    }

    public String getTimeSleep() {
        return TimeSleep;
    }

    public void setTimeSleep(String timeSleep) {
        TimeSleep = timeSleep;
    }

    public double getDayTemp() {
        return DayTemp;
    }

    public void setDayTemp(double dayTemp) {
        DayTemp = dayTemp;
    }

    public double getNightTemp() {
        return NightTemp;
    }

    public void setNightTemp(double nightTemp) {
        NightTemp = nightTemp;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getServer() {
        return Server;
    }

    public void setServer(String server) {
        Server = server;
    }

    /* Returns timestamp for waking time */
    public Date getTimeUpTS() {
        String[] time = TimeUp.split(":");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(time[1]));
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();

        //System.out.println("getTimeUpTS: ");
        //System.out.println("Hours: " + time[0] + " Minutes: " + time[1]);
        //System.out.println("Datum: ");
        //System.out.println(date);
        return date;
    }

    /* Returns timestamp for sleeping time */
    public Date getTimeSleepTS() {
        String[] time = TimeSleep.split(":");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(time[1]));
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();

        //System.out.println("getTimeSleepTS: ");
        //System.out.println("Hours: " + time[0] + " Minutes: " + time[1]);
        //System.out.println("Datum: ");
        //System.out.println(date);
        return date;
    }

}
