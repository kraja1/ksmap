import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import helpers.*;

public class SunInfo {
    private int Sunrise;
    private int Sunset;
    private int Clouds;
    private double Temperature;
    private String APIKey;
    private String City;

    public SunInfo() {
    }

    public SunInfo(String APIKey, String city) {
        this.APIKey = APIKey;
        City = city;
    }

    public int getSunrise() {
        return Sunrise;
    }

    public void setSunrise(int sunrise) {
        Sunrise = sunrise;
    }

    public int getSunset() {
        return Sunset;
    }

    public void setSunset(int sunset) {
        Sunset = sunset;
    }

    public int getClouds() {
        return Clouds;
    }

    public void setClouds(int clouds) {
        Clouds = clouds;
    }

    public String getAPIKey() {
        return APIKey;
    }

    public void setAPIKey(String APIKey) {
        this.APIKey = APIKey;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    /* Download data from OpenWeatherMap.org */
    public void downloadInfo() throws IOException {
        String url = Constant.SUNINFO_URL_BASE + APIKey + "&q=" + City;
        JSONObject data = getJSON(url);
        //System.out.println(data.toString());
        System.out.println("Weather data downloaded");

        JSONObject clouds = data.getJSONObject("clouds");
        JSONObject sys = data.getJSONObject("sys");
        JSONObject main = data.getJSONObject("main");
        /*System.out.println("Sunrise: " + sys.get("sunrise"));
        System.out.println("Sunset: " + sys.get("sunset"));
        System.out.println("Clouds: " + clouds.get("all"));
        System.out.println("temperature: " + main.get("temp"));*/

        Sunrise = Integer.parseInt(sys.get("sunrise").toString());
        Sunset = Integer.parseInt(sys.get("sunset").toString());
        Clouds = Integer.parseInt(clouds.get("all").toString());
        Temperature = Double.parseDouble(main.get("temp").toString());

        Logger.write("> Sunrise: " + this.getSunriseTS().toString() + ", Sunset: " + this.getSunsetTS().toString(),null);
        Logger.write("> Clouds: " + this.getClouds() + " Temperature: " + this.getTemperature(), null);
    }

    /* Get JSON from Given URL */
    private JSONObject getJSON(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        String jsonText = sb.toString();
        JSONObject json = new JSONObject(jsonText);
        is.close();
        return json;
    }

    /* Returns timestamp for sunrise */
    public Date getSunriseTS() {
        return new Date((long) Sunrise*1000);
    }

    /* Returns timestamp for sunset */
    public Date getSunsetTS() {
        return new Date((long) Sunset*1000);
    }

    public double getTemperature() {
        return Temperature;
    }

    public void setTemperature(double temperature) {
        Temperature = temperature;
    }
}
