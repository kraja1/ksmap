import helpers.Constant;
import helpers.Logger;
import io.javalin.Javalin;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

public class SmartDayCenter {
    private static SettingsState Settings = new SettingsState();
    private static HeatingState Heating = new HeatingState();
    private static LightState Light = new LightState();
    private static SunInfo Info = new SunInfo(Constant.SUNINFO_API_KEY, "Praha");

    /* Control variables for home automatics */
    private static boolean sunriseStarted = false;
    private static boolean sunsetStarted = false;
    private static boolean wakingStarted = false;
    private static boolean sleepingStarted = false;
    private static boolean definitelyDay = false;
    private static boolean definitelyNight = false;

    private static boolean sunriseDone = false;
    private static boolean sunsetDone = false;
    private static boolean wakingDone = false;
    private static boolean sleepingDone = false;

    private static Date sunsetStart = new Date();
    private static Date sunriseStart = new Date();
    private static Date wakeStart = new Date();
    private static Date sleepStart = new Date();

    private static int sunriseCounter = 0;
    private static int sunsetCounter = 0;
    private static int wakingCounter = 0;
    private static int sleepingCounter = 0;


    /* Variables for testing */
    private static Timer quickTimer = new Timer();
    private static Date quickTime;
    /* Methods for testing */
    private static void initiateQuickTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, Constant.QUICKTIME_START_H);
        cal.set(Calendar.MINUTE, Constant.QUICKTIME_START_M);
        cal.set(Calendar.SECOND, Constant.QUICKTIME_START_S);
        quickTime = cal.getTime();
    }
    private static class quickenTime extends TimerTask {
        public void run() {
            Calendar cal = Calendar.getInstance();
            cal.setTime(quickTime);
            cal.add(Calendar.SECOND,Constant.QUICKTIME_MUL_COEFF);
            quickTime = cal.getTime();
            System.out.println("Time quickened:" + quickTime);
        }
    }

    /* Timer task for weather and sun information update */
    private static class SunInfoUpdate extends TimerTask {
        public void run() {
            try {
                //Date now = quickTime;
                Date now = new Date();
                Logger.write("----- " + now.toString() + " ----- SunInfoUpdate -----", null);
                Info.downloadInfo();
                /* Correct times for sunrise and sunset actions starts */
                Calendar cal = Calendar.getInstance();
                cal.setTime(Info.getSunsetTS());
                cal.add(Calendar.SECOND, (int) (Constant.SUNSET_SHIFT_BASE * ((double)Info.getClouds()/2+Constant.SUNSET_SHIFT_CLOUDS_SHIFT)/100f));
                sunsetStart = cal.getTime();
                cal.setTime(Info.getSunriseTS());
                cal.add(Calendar.SECOND, (int) (Constant.SUNRISE_SHIFT_BASE * (1f-((Info.getClouds() + Constant.SUNRISE_SHIFT_CLOUDS_SHIFT)/100f))));
                sunriseStart = cal.getTime();
                //System.out.println("Sunrise: " + Info.getSunriseTS() + ", sunriseStart: " + sunriseStart.toString() + " Diff: " + (int) (-900f * (1f-((double)Info.getClouds()/100f))));
                Logger.write("> SunriseActionsStart: " + sunriseStart.toString() + ", SunsetActionStart: " + sunsetStart.toString(), null);

            } catch (IOException e) {
                System.out.println("Downloading of sun info failed");
                Logger.write("ERROR> Weather and sun info download failed.", null);
            }

        }
    }

    /* Timer task for smart endpoints state check */
    private static class EndpointsInfoUpdate extends TimerTask {
        public void run() {

            //Date now = quickTime;
            Date now = new Date();
            Logger.write("----- " + now.toString() + " ----- EndpointsInfoUpdate -----", null);

            /* Temperature change simulations for testing without real endpoints.
            *  Comment out for real use*/
            Random rand = new Random();
            if(Info.getTemperature() < Heating.getSetTemp() - Constant.TEMPERATURE_DIFF_COLD && Light.getWindowOpen()){
                if(rand.nextDouble() <= 0.75){
                    Heating.setCurTemp(Heating.getCurTemp() - 1.5);
                    Heating.setCurTemp(Math.round(Heating.getCurTemp()*10/10.0));
                }
            }
            /* *** *** */

            Heating.updateState();
            Light.updateState();
        }
    }

    /* Main method for automatic smart home control
    *  check what is the time a do appropriate actions */
    private static class StatusUpdate extends TimerTask {
        public void run() {

            //Date now = quickTime/*new Date()*/;
            Date now = new Date();
            Logger.write("----- " + now.toString() + " ----- Status Update Check -----", null);
            /* Is some automatics turned on at all? */
            if (Light.getAutoLight() || Light.getWindowAuto() || Light.getShadeAuto() || Heating.isHeatingAuto()) {
                Logger.write("----- " + now.toString() + " ----- Status Update Check: Automatics ON -----", null);

                /* Time independent tasks*/
                /* Checks for case of the automatic control being newly switched on */
                if (definitelyDay && !Light.getShadeOpen() && Light.getShadeAuto() && !sleepingStarted && !sleepingDone) { //automatics newly switched on
                    Logger.write("----- " + now.toString() + " ----- Action Code 006: open shades (automatics newly switched on) -----", null);
                    Light.openShade();
                }
                if (definitelyNight && Light.getShadeOpen() && Light.getShadeAuto()) {
                    Logger.write("----- " + now.toString() + " ----- Action Code 007: close shades (automatics newly switched on) -----", null);
                    Light.closeShade();
                }
                if (definitelyDay && Light.getLight() && Light.getAutoLight()) { //automatics newly switched on
                    Logger.write("----- " + now.toString() + " ----- Action Code 008: turn off lights (automatics newly switched on) -----", null);
                    Light.switchLight(false);
                }
                if (definitelyNight && !Light.getLight() && Light.getAutoLight() && !sleepingDone) { //automatics newly switched on
                    Logger.write("----- " + now.toString() + " ----- Action Code 009: turn on lights (automatics newly switched on) -----", null);
                    Light.switchLight(true);
                }

                /* Is outside warm enough? Turn heating off */
                if (Info.getTemperature() >= Heating.getSetTemp() && Heating.isHeating()) {
                    if (Heating.isHeatingAuto()) {
                        Logger.write("----- " + now.toString() + " ----- Action Code 001: switch heating off (outdoor temperature high) -----", null);
                        Heating.toggleHeating(false);
                    }
                }
                /* Is outside warm enough and not too hot? Open windows */
                if (Info.getTemperature() > Heating.getSetTemp() && !(Info.getTemperature() > (Heating.getSetTemp() + Constant.TEMPERATURE_DIFF_HEAT))) {
                    if (Light.getWindowAuto() && !Light.getWindowOpen()) {
                        Logger.write("----- " + now.toString() + " ----- Action Code 002: open windows (outdoor temperature fine) -----", null);
                        Light.openWindow();
                    }
                }
                /* Is lower temperature requested and outside temperature lower than current? Open windows
                 * (not if sleeping preparations started - the windows are taken care of) */
                if (Heating.getSetTemp() < (Heating.getCurTemp() - Constant.TEMPERATURE_DIFF_TOLERANCE) && Info.getTemperature() < Heating.getSetTemp()) {
                    if (Light.getWindowAuto() && !Light.getWindowOpen() && !sleepingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Action Code 003: open windows (to cool down) -----", null);
                        Light.openWindow();
                    }
                }
                /* Is outside too cold? Turn heating on */
                if (Info.getTemperature() <= (Heating.getSetTemp() - Constant.TEMPERATURE_DIFF_COLD)) {
                    if (Heating.isHeatingAuto() && !Heating.isHeating()) {
                        Logger.write("----- " + now.toString() + " ----- Action Code 004: switch heating on (outdoor temperature low)-----", null);
                        Heating.toggleHeating(true);
                    }
                }
                /* Is it too cold or too hot both inside and outside? Close windows if opened */
                if ((Heating.getCurTemp() <= (Heating.getSetTemp() - Constant.TEMPERATURE_DIFF_TOLERANCE) && Info.getTemperature() < Heating.getSetTemp()) || (Heating.getCurTemp() >= (Heating.getSetTemp() + Constant.TEMPERATURE_DIFF_TOLERANCE) && Info.getTemperature() > Heating.getSetTemp() + Constant.TEMPERATURE_DIFF_HEAT)) {
                    if (Light.getWindowAuto() && Light.getWindowOpen() && !sleepingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Action Code 005: close windows (too cold or too hot) -----", null);
                        Light.closeWindow();
                    }
                }

                /* Time dependant tasks */

                /* Sunrise actions */
                if ((Math.abs(now.getTime() - sunriseStart.getTime()) <= Constant.ACTION_START_TOLERANCE) || sunriseStarted) {
                    Logger.write("----- " + now.toString() + " ----- Sunrise actions -----", null);
                    if (wakingDone || wakingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Sunrise: waking done or in progress -----", null);
                        if(Light.getAutoLight()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 006: dim light (sunrise) -----", null);
                            /* Dim light */
                            Light.changeBrightnessRel(-1);
                        }
                        if (!sunriseStarted) {
                            Logger.write("----- " + now.toString() + " ----- Sunrise: waking done or in progress and first action: open shades -----", null);
                            if(Light.getShadeAuto()){
                                Logger.write("----- " + now.toString() + " ----- Action Code 007: open shades (user is awake) -----", null);
                                /* Open shades */
                                Light.openShade();
                            }
                        }
                    }
                    sunriseStarted = true;
                    definitelyNight = false;
                    sunriseCounter++;
                    /* End of sunrise Actions */
                    if (sunriseCounter > Constant.SUNRISE_COUNTER_MAX) {
                        Logger.write("----- " + now.toString() + " ----- Sunrise actions end -----", null);
                        sunriseStarted = false;
                        sunriseDone = true;
                        sunsetDone = false;
                        sunriseCounter = 0;
                    }
                }

                /* Sunset actions */
                if ((Math.abs(now.getTime() - sunsetStart.getTime()) <= Constant.ACTION_START_TOLERANCE) || sunsetStarted) {
                    Logger.write("----- " + now.toString() + " ----- Sunset actions -----", null);
                    if (!sleepingDone && !sleepingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Sunset: not sleeping yet -----", null);
                        if(Light.getAutoLight()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 008: brighten light (sunset)-----", null);
                            /* First evening action - switch the light on at lowest brightness */
                            if(!sunsetStarted){
                                Light.switchLight(true);
                                Light.changeBrightnessAbs(1);
                            }
                            /* Brighten light */
                            Light.changeBrightnessRel(1);
                        }
                    }
                    sunsetStarted = true;
                    definitelyDay = false;
                    sunsetCounter++;
                    /* End of sunset actions */
                    if (sunsetCounter > Constant.SUNSET_COUNTER_MAX) {
                        Logger.write("----- " + now.toString() + " ----- Sunset actions end -----", null);
                        if(Light.getShadeAuto()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 009: close shades (last sunset action)-----", null);
                            /* Close shades as the last sunset action */
                            Light.closeShade();
                        }
                        sunsetStarted = false;
                        sunsetDone = true;
                        sunriseDone = false;
                        sunsetCounter = 0;
                    }
                }

                //Logger.write("Before Waking IF: Difference" + Math.abs(now.getTime() - wakeStart.getTime()) + " wakeStart: " + wakeStart.toString(), null);
                /* Waking actions */
                if ((Math.abs(now.getTime() - wakeStart.getTime()) <= Constant.ACTION_START_TOLERANCE) || wakingStarted) {
                    Logger.write("----- " + now.toString() + " ----- Waking actions -----", null);
                    /* First waking actions */
                    if (!wakingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Waking: first action: heating day and prepare light -----", null);
                        /* Set the heating to day temperature */
                        if(Heating.isHeatingAuto())Heating.changeTemperature(Settings.getDayTemp());
                        /* Switch the light on, but on zero brightness for start */
                        if(Light.getAutoLight())Light.changeBrightnessAbs(0);
                        if(Light.getAutoLight())Light.switchLight(true);
                    }
                    wakingStarted = true;
                    wakingCounter++;
                    /* Is it before sunrise? */
                    if (!sunriseDone && !sunriseStarted && wakingCounter > 4) {
                        Logger.write("----- " + now.toString() + " ----- Waking: before sunrise -----", null);
                        if(Light.getAutoLight()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 010: brighten light (to wake more easily)-----", null);
                            /* Brighten light */
                            Light.changeBrightnessRel(2);
                        }
                    }
                    /* Is at after or during sunrise and is time for shades opening? */
                    if (wakingCounter == Constant.WAKING_COUNTER_SHADE && (sunriseDone || sunriseStarted)) {
                        Logger.write("----- " + now.toString() + " ----- Waking: during or after sunrise -----", null);
                        if(Light.getShadeAuto()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 011: open shades (waking during or after sunrise) -----", null);
                            /* Open shades */
                            Light.openShade();
                        }
                    }
                    /* End of waking actions */
                    if (wakingCounter > Constant.WAKING_COUNTER_MAX) {
                        Logger.write("----- " + now.toString() + " ----- Waking actions end -----", null);
                        wakingStarted = false;
                        wakingDone = true;
                        sleepingDone = false;
                        wakingCounter = 0;
                    }
                }

                /* Sleeping preparation actions */
                if ((Math.abs(now.getTime() - sleepStart.getTime()) <= Constant.ACTION_START_TOLERANCE) || sleepingStarted) { //usinani
                    Logger.write("----- " + now.toString() + " ----- Sleeping actions -----", null);
                    /* First action */
                    if (!sleepingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Sleeping preparations: first action: heating night -----", null);
                        /* Set the heating to night temperature */
                        if(Heating.isHeatingAuto())Heating.changeTemperature(Settings.getNightTemp());
                    }
                    sleepingStarted = true;
                    sleepingCounter++;
                    /* Is it after or during sunset? (and action step control and others)
                    *  don't turn the light completely off - that's up to user*/
                    if (sleepingCounter % Constant.SLEEPING_COUNTER_STEP == 0 && Light.getBrightness() > Constant.MIN_LIGHT_BRIGHTNESS && (sunsetDone || sunsetStarted)) {
                        Logger.write("----- " + now.toString() + " ----- Sleeping preparations: soften light -----", null);
                        if(Light.getAutoLight()){
                            /* Correction for current brightness*/
                            if(Light.getBrightness() > (20 - (sleepingCounter/2))){
                                Logger.write("----- " + now.toString() + " ----- Action Code 012: dim light (sleeping preparations) -----", null);
                                /* Dim lights */
                                Light.changeBrightnessRel(-1);
                            }
                        }
                    }
                    /* Is the time for shades closing? (In case the sun's still shining) */
                    if (sleepingCounter == Constant.SLEEPING_COUNTER_SHADE && !sunsetDone) {
                        Logger.write("----- " + now.toString() + " ----- Sleeping preparations: in the middle, close shades -----", null);
                        if(Light.getShadeAuto()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 013: close shades (sleeping before complete darkness) -----", null);
                            /* Close shades */
                            Light.closeShade();
                        }
                    }
                    /* Is the time for evening window opening? */
                    if(sleepingCounter == Constant.SLEEPING_COUNTER_WINDOW_OPEN){
                        Logger.write("----- " + now.toString() + " ----- Sleeping preparations: in the middle, open windows -----", null);
                        if(Light.getWindowAuto()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 018: open windows (for some evening air)-----", null);
                            /* Open windows */
                            Light.openWindow();
                        }
                    }
                    /* Is the temperature fine and have the windows been opened long enough?
                    *  (if the temperature is not too low, the windows can stay open) */
                    if(sleepingCounter == Constant.SLEEPING_COUNTER_WINDOW_CLOSE && Heating.getCurTemp() < Heating.getSetTemp() - Constant.TEMPERATURE_DIFF_TOLERANCE){
                        Logger.write("----- " + now.toString() + " ----- Action Code 019: close windows (end of evening air) -----", null);
                        /* Close windows */
                        if(Light.getWindowAuto())Light.closeWindow();
                    }
                    /* End of sleeping preparation actions */
                    if (sleepingCounter > Constant.SLEEPING_COUNTER_MAX) {
                        Logger.write("----- " + now.toString() + " ----- Sleeping actions end -----", null);
                        sleepingStarted = false;
                        sleepingDone = true;
                        wakingDone = false;
                        sleepingCounter = 0;
                    }
                }

                /* Safety checks for when it is definitely day or night */
                if (wakingDone && sunriseDone && !definitelyDay && !sunsetStarted) {
                    Logger.write("----- " + now.toString() + " ----- Definitely day -----", null);
                    if(Light.getAutoLight() && Light.getLight()){
                        Logger.write("----- " + now.toString() + " ----- Action Code 014: light off (definitely day) -----", null);
                        /* Switch the light off */
                        Light.switchLight(false);
                    }
                    if(Light.getShadeAuto() && !Light.getShadeOpen()){
                        Logger.write("----- " + now.toString() + " ----- Action Code 015: open shades (definitely day) -----", null);
                        /* Close shades */
                        Light.openShade();
                    }
                    definitelyDay = true;
                    definitelyNight = false;
                }
                if (sunsetDone && !sleepingStarted && !definitelyNight && !sunriseStarted) {
                    Logger.write("----- " + now.toString() + " ----- Definitely night -----", null);
                    /* It is definitely dark, but the user is awake */
                    if (!sleepingDone && !sleepingStarted) {
                        Logger.write("----- " + now.toString() + " ----- Definitely night: not yet sleeping -----", null);
                        if(Light.getAutoLight()){
                            Logger.write("----- " + now.toString() + " ----- Action Code 016: light on (definitely dark) -----", null);
                            /* Switch the light on */
                            Light.switchLight(true);
                            Light.changeBrightnessAbs(Constant.MAX_LIGHT_BRIGHTNESS);
                        }
                    }
                    Logger.write("----- " + now.toString() + " ----- Definitely night:close shades -----", null);
                    if(Light.getShadeAuto() && Light.getShadeOpen()){
                        Logger.write("----- " + now.toString() + " ----- Action Code 017: close shades (definitely dark)-----", null);
                        /* Close shades */
                        Light.closeShade();
                    }
                    definitelyNight = true;
                    definitelyDay = false;
                }

            }
        }
    }

    /* Waking actions start time calculation */
    private static Date wakingTime(Date alarm) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(alarm);
        cal.add(Calendar.MINUTE, -Constant.WAKING_SHIFT);
        return cal.getTime();
    }

    /* Sleeping preparation actions start time calculation */
    private static Date sleepingTime(Date alarm) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(alarm);
        cal.add(Calendar.MINUTE, -Constant.SLEEPING_SHIFT);
        return cal.getTime();
    }

    private static void controlInit() {
        Date now = new Date();
        if(now.compareTo(Info.getSunriseTS())>0 &&
           now.compareTo(Settings.getTimeUpTS())>0 &&
           now.compareTo(Settings.getTimeSleepTS())<0 &&
           now.compareTo(Info.getSunsetTS())<0){
            definitelyDay = true;
            System.out.println("definitelyDay");
        }
        if(now.compareTo(Info.getSunriseTS())>0 &&
           now.compareTo(Info.getSunsetTS())<0){
            sunriseDone = true;
            System.out.println("sunriseDone");
        }
        if(now.compareTo(Settings.getTimeUpTS())>0 && now.compareTo(Settings.getTimeSleepTS())<0){
            wakingDone = true;
            System.out.println("wakingDone");
        }

        if((now.compareTo(Info.getSunriseTS())<0 ||
            now.compareTo(Info.getSunsetTS())>0) &&
           (now.compareTo(Settings.getTimeUpTS())<0 ||
            now.compareTo(Settings.getTimeSleepTS())>0)){
            definitelyNight = true;
            System.out.println("definitelyNight");
        }
        if(now.compareTo(Info.getSunriseTS())<0 ||
                now.compareTo(Info.getSunsetTS())>0){
            sunsetDone = true;
            System.out.println("sunsetDone");
        }
        if(now.compareTo(Settings.getTimeUpTS())<0 || now.compareTo(Settings.getTimeSleepTS())>0){
            sleepingDone = true;
            System.out.println("sleepingDone");
        }
    }

    public static void main(String[] args) {
        //first endpoint check
        Heating.updateState();
        Light.updateState();

        //first weather info download
        try {
            Info.downloadInfo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //control variables initialization
        controlInit();

        //first time shift calculations
        wakeStart = wakingTime(Settings.getTimeUpTS());
        sleepStart = sleepingTime(Settings.getTimeSleepTS());
        Logger.write("wakeStart: " + wakeStart.toString() + " sleepStart: " + sleepStart.toString(), null);

        //server start
        Javalin app = Javalin.create().start("0.0.0.0", Constant.SERVER_PORT);
        app.get("/", ctx -> ctx.result("Your SmartDayCenter is up and running. Use you SmartDay Android app for control."));

        /* Received GET request for heating info */
        app.get("/heating", ctx -> {

            System.out.println("App asked for heating");
            //Date now = quickTime/*new Date()*/;
            Date now = new Date();

            Logger.write("----- " + now.toString() + " ----- App asked for heating info-----", null);
            HeatingState send = new HeatingState(Heating);

            ctx.json(send);
        });

        /* Received GET request for light info */
        app.get("/light", ctx -> {

            System.out.println("App asked for light");
            //Date now = /*new Date()*/quickTime;
            Date now = new Date();

            Logger.write("----- " + now.toString() + " ----- App asked for light info-----", null);
            LightState send = new LightState(Light);

            ctx.json(send);
        });

        /* Received GET request for settings info */
        app.get("/settings", ctx -> {

            System.out.println("App asked for settings");
            //Date now = quickTime/*new Date()*/;
            Date now = new Date();

            Logger.write("----- " + now.toString() + " ----- App asked for settings info -----", null);
            SettingsState send = new SettingsState(Settings);

            ctx.json(send);
        });

        /* Received POST request with light change info */
        app.post("/light", ctx -> {
            //System.out.println(ctx.body());
            System.out.println("App sent new light settings");
            //Date now = quickTime/*new Date()*/;
            Date now = new Date();
            Logger.write("----- " + now.toString() + " ----- App sent light change-----", null);
            LightState received = ctx.bodyAsClass(LightState.class);

            Light.changeState(received);

            JSONObject send = new JSONObject();
            send.put("response", "OK");
            ctx.json(send);
        });

        /* Received POST request with heating change info */
        app.post("/heating", ctx -> {
            //System.out.println(ctx.body());
            System.out.println("App sent new heating settings");
            //Date now = quickTime/*new Date()*/;
            Date now = new Date();
            Logger.write("----- " + now.toString() + " ----- App sent heating change -----", null);
            HeatingState received = ctx.bodyAsClass(HeatingState.class);

            Heating.changeState(received);

            JSONObject send = new JSONObject();
            send.put("response", "OK");
            ctx.json(send);
        });

        /* Received POST request with settings change info */
        app.post("/settings", ctx -> {
            //System.out.println(ctx.body());
            System.out.println("App sent new general settings");
            //Date now = quickTime/*new Date()*/;
            Date now = new Date();
            Logger.write("----- " + now.toString() + " ----- App sent new settings -----", null);
            SettingsState received = ctx.bodyAsClass(SettingsState.class);

            if(!Settings.getTimeUp().equals(received.getTimeUp())){
                Settings.setTimeUp(received.getTimeUp());
                //update waking time
                wakeStart = wakingTime(received.getTimeUpTS());
            }
            if(!Settings.getTimeSleep().equals(received.getTimeSleep())){
                Settings.setTimeSleep(received.getTimeSleep());
                //update sleeping time
                sleepStart = sleepingTime(received.getTimeSleepTS());
            }
            if(!Settings.getCity().equals(received.getCity())){
                Settings.setCity(received.getCity());
                Info.setCity(Settings.getCity());
                //update suninfo
                Info.downloadInfo();
            }
            if(!Settings.getServer().equals(received.getServer())){
                Settings.setServer(received.getServer());
                //should not happen
            }
            if(Settings.getDayTemp() != received.getDayTemp()){
                Settings.setDayTemp(received.getDayTemp());
                //update heating if necessary
                if(wakingStarted || (wakingDone && !sleepingStarted)){
                    Logger.write("----- " + now.toString() + " ----- App sent new day temperature and it is daytime -----", null);
                    Heating.changeTemperature(Settings.getDayTemp());
                }

            }
            if(Settings.getNightTemp() != received.getNightTemp()){
                Settings.setNightTemp(received.getNightTemp());
                //update heating if necessary
                if(sleepingStarted || (sleepingDone && !wakingStarted)){
                    Logger.write("----- " + now.toString() + " ----- App sent new night temperature and it is nighttime -----", null);
                    Heating.changeTemperature(Settings.getNightTemp());
                }

            }

            JSONObject send = new JSONObject();
            send.put("response", "OK");
            ctx.json(send);
        });

        /* Quick time for testing */
        //System.out.println("Starting QuickTime");
        //initiateQuickTime();
        //quickTimer.schedule(new quickenTime(), 0,Constant.QUICKTIME_MUL_PERIOD);

        //Timer quickSunTimer = new Timer();
        //quickSunTimer.schedule(new SunInfoUpdate(), 250, Constant.SUNINFO_TIMER_QUICKPERIOD);
        //Timer quickEndpoints = new Timer();
        //quickEndpoints.schedule(new EndpointsInfoUpdate(),5000, Constant.ENDPOINT_TIMER_QUICKPERIOD);
        //Timer quickDay = new Timer();
        //quickDay.schedule(new StatusUpdate(), 1000, Constant.ACTION_TIMER_QUICKPERIOD);

        Timer sunTimer = new Timer();
        sunTimer.schedule(new SunInfoUpdate(), 250, Constant.SUNINFO_TIMER_PERIOD);
        Timer endpoints = new Timer();
        endpoints.schedule(new EndpointsInfoUpdate(),5000, Constant.ENDPOINT_TIMER_PERIOD);
        Timer day = new Timer();
        day.schedule(new StatusUpdate(), 2000, Constant.ACTION_TIMER_PERIOD);
    }
}
