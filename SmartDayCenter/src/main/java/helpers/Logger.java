package helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
    public static void write(String message, String file) {
        String fileName = Constant.LOGFILE;
        if(file != null) fileName = file;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))) {
            bw.write(message);
            bw.newLine();
            bw.flush();
        } catch(IOException e){
            System.err.println("Can't write into log.");
        }

    }
}
