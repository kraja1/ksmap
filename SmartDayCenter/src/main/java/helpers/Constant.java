package helpers;

public class Constant {
    public static final String SUNINFO_API_KEY = "b8a6c01493b404379ae45b2ee1d9b774";
    public static final String SUNINFO_URL_BASE = "https://api.openweathermap.org/data/2.5/weather?mode=json&units=metric&appid=";
    public static final int QUICKTIME_START_H = 7; //6
    public static final int QUICKTIME_START_M = 0;
    public static final int QUICKTIME_START_S = 0;
    public static final int QUICKTIME_MUL_COEFF = 30;
    public static final int QUICKTIME_MUL_PERIOD = 500;
    public static final double SUNSET_SHIFT_BASE = -2500f;
    public static final double SUNSET_SHIFT_CLOUDS_SHIFT = 20f;
    public static final double SUNRISE_SHIFT_BASE = -1200f;
    public static final double SUNRISE_SHIFT_CLOUDS_SHIFT = -10f;
    public static final String LOGFILE = "log.txt";
    public static final double TEMPERATURE_DIFF_HEAT = 7;
    public static final double TEMPERATURE_DIFF_TOLERANCE = 0.5;
    public static final double TEMPERATURE_DIFF_COLD = 5;
    public static final int ACTION_START_TOLERANCE = 60000;
    public static final int SUNRISE_COUNTER_MAX = 20;
    public static final int SUNSET_COUNTER_MAX = 20;
    public static final int WAKING_COUNTER_MAX = 20;
    public static final int WAKING_COUNTER_SHADE = 5;
    public static final int SLEEPING_COUNTER_MAX = 40;
    public static final int SLEEPING_COUNTER_SHADE = 30;
    public static final int SLEEPING_COUNTER_STEP = 2;
    public static final int SLEEPING_COUNTER_WINDOW_OPEN = 20;
    public static final int SLEEPING_COUNTER_WINDOW_CLOSE = 35;
    public static final int WAKING_SHIFT = 15;
    public static final int SLEEPING_SHIFT = 60;
    public static final int SERVER_PORT = 54321;//7320
    public static final int SUNINFO_TIMER_QUICKPERIOD = 30000;
    public static final int ACTION_TIMER_QUICKPERIOD = 1500;
    public static final int ENDPOINT_TIMER_QUICKPERIOD = 15000;
    public static final int SUNINFO_TIMER_PERIOD = 1800000;
    public static final int ACTION_TIMER_PERIOD = 90000;
    public static final int ENDPOINT_TIMER_PERIOD = 300000;
    public static final double DEAFULT_HEATING_CURTEMP = 21;
    public static final double DEFAULT_HEATING_SETTEMP = 18.5;
    public static final boolean DEFAULT_HEATING_ON = true;
    public static final boolean DEFAULT_HEATING_AUTO = true;
    public static final String DEFAULT_SETTINGS_WAKE = "06:30"; //0630
    public static final String DEFAULT_SETTINGS_SLEEP = "22:30"; //2230
    public static final double DEFAULT_SETTINGS_DAYTEMP = 21;
    public static final double DEFAULT_SETTINGS_NIGHTTEMP = 18;
    public static final String DEFAULT_SETTINGS_CITY = "Praha";
    public static final String DEFAULT_SETTINGS_SERVER = "192.168.0.213";
    public static final boolean DEFAULT_LIGHT_ON = false;
    public static final int DEFAULT_LIGHT_BRIGHTNESS = 10;
    public static final int MIN_LIGHT_BRIGHTNESS = 1;
    public static final int MAX_LIGHT_BRIGHTNESS = 20;
    public static final boolean DEFAULT_LIGHT_AUTO = true;
    public static final boolean DEFAULT_WINDOW_OPEN = false;
    public static final boolean DEFAULT_WINDOW_AUTO = true;
    public static final boolean DEFAULT_SHADE_OPEN = false;
    public static final boolean DEFAULT_SHADE_AUTO = true;
    public static final String  HEATING_BASE_URL = "heating";
    public static final String  LIGHT_BASE_URL = "light";

}
