This is a project for KIT-KSMAP subject on FIM UHK

Smart day is a system for central smart home control.

It is prepared for control of smart lights, heating, windows and shades
with the main goal beeing support for easier waking, falling asleep and 
overall better sleep.

The system consist of Android App and Java server working as a main hub.

Directory SmartDayApp contents code of SmartDay application for Android OS

Directory SmartDayCenter contents code of SmarDay server application

