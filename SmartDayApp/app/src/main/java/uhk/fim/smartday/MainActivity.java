package uhk.fim.smartday;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import uhk.fim.smartday.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        /* Icon made by https://www.flaticon.com/authors/freepik" */
        tabs.getTabAt(0).setIcon(R.drawable.lightbulb);
        /* Icon made by https://www.flaticon.com/authors/dinosoftlabs */
        tabs.getTabAt(1).setIcon(R.drawable.thermometer_1);
        /* Icon made by https://smashicons.com/ */
        tabs.getTabAt(2).setIcon(R.drawable.settings);
    }
}