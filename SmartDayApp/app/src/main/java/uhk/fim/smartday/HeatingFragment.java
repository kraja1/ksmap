package uhk.fim.smartday;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HeatingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HeatingFragment extends Fragment {


    private Button plusBT;// = (Button) findViewById(R.id.buttonPlus);
    private Button minusBT;
    private TextView setTempTV;
    private TextView curTempTV;
    private SwitchCompat heatingSW;
    private SwitchCompat autoSW;

    public HeatingFragment() {
        // Required empty public constructor
    }

    public static HeatingFragment newInstance() {
        HeatingFragment fragment = new HeatingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_heating, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        plusBT = (Button) getView().findViewById(R.id.buttonPlus);
        minusBT = (Button) getView().findViewById(R.id.buttonMinus);
        setTempTV = (TextView) getView().findViewById(R.id.textViewSetTempVal);
        curTempTV = (TextView) getView().findViewById(R.id.textViewCurTempVal);
        heatingSW = (SwitchCompat) getView().findViewById(R.id.switchHeating);
        autoSW = (SwitchCompat) getView().findViewById(R.id.switchAutoHeating);

        /* Load data from SmartDayCenter  */
        loadData();

        plusBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double setTemp = getLocalSetTempVal();
                setTemp += 0.5;
                String temp = String.valueOf(setTemp);
                setTempTV.setText(temp + " °C");
                uploadState();
            }
        });

        minusBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double setTemp = getLocalSetTempVal();
                setTemp -= 0.5;
                String temp = String.valueOf(setTemp);
                setTempTV.setText(temp + " °C");
                uploadState();
            }
        });

        heatingSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!heatingSW.isChecked()) {
                    //also turn off autoHeating
                    autoSW.setTag("loading");
                    autoSW.setChecked(false);
                    autoSW.setTag(null);
                }
                uploadState();
            }
        });

        autoSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(autoSW.isChecked()) {
                    //also turn on heating
                    heatingSW.setTag("loading");
                    heatingSW.setChecked(true);
                    heatingSW.setTag(null);
                }

                uploadState();
            }
        });

    }

    /* Parsing method for °C */
    double getLocalSetTempVal() {
        String temp = setTempTV.getText().toString();
        temp = temp.trim().replace(" °C", "");
        return Double.parseDouble(temp);
    }

    /* Download data from SmartDay Center */
    void loadData(){
        RequestQueue que = Volley.newRequestQueue(getContext());
        que.start();

        //String URL = "http://192.168.0.213:54321/heating";
        // Use saved server URL
        SharedPreferences shPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String URL = "http://" + shPref.getString("server", "192.168.0.213") + ":54321/heating";


        JsonObjectRequest JSORequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                double CurTemp=0;
                double SetTemp=0;
                boolean heating=false;
                boolean heatingAuto=false;
                try {
                    // Read JSON
                    CurTemp = response.getDouble("curTemp");
                    SetTemp = response.getDouble("setTemp");
                    heating = response.getBoolean("heating");
                    heatingAuto = response.getBoolean("heatingAuto");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Set tags not to trigger change listeners
                setTempTV.setTag("loading");
                curTempTV.setTag("loading");
                heatingSW.setTag("loading");
                autoSW.setTag("loading");
                curTempTV.setText(String.valueOf(CurTemp) + " °C");
                setTempTV.setText(String.valueOf(SetTemp) + " °C");
                heatingSW.setChecked(heating);
                autoSW.setChecked(heatingAuto);
                setTempTV.setTag(null);
                curTempTV.setTag(null);
                heatingSW.setTag(null);
                autoSW.setTag(null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Server není dostupný", Toast.LENGTH_LONG).show();
            }
        });

        que.add(JSORequest);
    }

    /* Save data to SmartDayCenter */
    private void uploadState() {
        // Create JSON content
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("curTemp", curTempTV.getText().toString().trim().replace(" °C",""));
        params.put("setTemp", setTempTV.getText().toString().trim().replace(" °C",""));
        params.put("heating", String.valueOf(heatingSW.isChecked()));
        params.put("heatingAuto", String.valueOf(autoSW.isChecked()));

        //String URL = "http://192.168.0.213:54321/heating";
        SharedPreferences shPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String URL = "http://" + shPref.getString("server", "192.168.0.213") + ":54321/heating";


        RequestQueue que = Volley.newRequestQueue(getContext());
        que.start();

        JsonObjectRequest JSORequest = new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Server není dostupný", Toast.LENGTH_LONG).show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        que.add(JSORequest);
    }

    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(getContext(), "on resume heating", Toast.LENGTH_SHORT).show();
        loadData();
    }
}