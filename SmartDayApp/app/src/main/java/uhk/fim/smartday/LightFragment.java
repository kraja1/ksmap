package uhk.fim.smartday;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LightFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LightFragment extends Fragment {

    private SwitchCompat lightSW;
    private SeekBar brightSB;
    private SwitchCompat autoLightSW;
    private ToggleButton windowTB;
    private ToggleButton shadeTB;
    private SwitchCompat autoWindowSW;
    private SwitchCompat autoShadeSW;

    public LightFragment() {
        // Required empty public constructor
    }


    public static LightFragment newInstance() {
        LightFragment fragment = new LightFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_light, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        lightSW = (SwitchCompat) getView().findViewById(R.id.switchLight);
        brightSB = (SeekBar) getView().findViewById(R.id.seekBarBright);
        autoLightSW = (SwitchCompat) getView().findViewById(R.id.switchAutoLight);
        windowTB = (ToggleButton) getView().findViewById(R.id.toggleButtonWin);
        shadeTB = (ToggleButton) getView().findViewById(R.id.toggleButtonShade);
        autoWindowSW = (SwitchCompat) getView().findViewById(R.id.switchAutoWin);
        autoShadeSW = (SwitchCompat) getView().findViewById(R.id.switchAutoShades);

        /* Load data from SmartDayCenter */
        loadData();

        lightSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!lightSW.isChecked()) {
                    //also turn of autoLight
                    autoLightSW.setTag("loading");
                    autoLightSW.setChecked(false);
                    autoLightSW.setTag(null);
                }
                uploadState();
            }
        });

        autoLightSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadState();
            }
        });

        windowTB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadState();
            }
        });

        shadeTB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadState();
            }
        });

        autoWindowSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadState();
            }
        });

        autoShadeSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadState();
            }
        });

        brightSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    brightSB.setTag("user");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(brightSB.getTag()=="user"){
                    uploadState();
                    brightSB.setTag(null);
                }
            }
        });

    }

    /* Download fragment data from server */
    private void loadData(){

        RequestQueue que = Volley.newRequestQueue(getContext());
        que.start();

        //String URL = "http://192.168.0.213:54321/light";
        // Use saved server URL
        SharedPreferences shPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String URL = "http://" + shPref.getString("server", "192.168.0.213") + ":54321/light";

        JsonObjectRequest JSORequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                boolean Light=false;
                boolean AutoLight=false;
                boolean Window=false;
                boolean Shade=false;
                boolean WindowAuto=false;
                boolean ShadeAuto=false;
                int Bright=0;
                try {
                    // Read JSON
                    Light = response.getBoolean("light");
                    AutoLight = response.getBoolean("autoLight");
                    Window = response.getBoolean("windowOpen");
                    Shade = response.getBoolean("shadeOpen");
                    WindowAuto = response.getBoolean("windowAuto");
                    ShadeAuto = response.getBoolean("shadeAuto");
                    Bright = response.getInt("brightness");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Set tags not to trigger change listeners
                lightSW.setTag("loading");
                brightSB.setTag("loading");
                autoLightSW.setTag("loading");
                windowTB.setTag("loading");
                shadeTB.setTag("loading");
                autoWindowSW.setTag("loading");
                autoShadeSW.setTag("loading");
                lightSW.setChecked(Light);
                brightSB.setProgress(Bright);
                autoLightSW.setChecked(AutoLight);
                windowTB.setChecked(Window);
                shadeTB.setChecked(Shade);
                autoWindowSW.setChecked(WindowAuto);
                autoShadeSW.setChecked(ShadeAuto);
                lightSW.setTag(null);
                brightSB.setTag(null);
                autoLightSW.setTag(null);
                windowTB.setTag(null);
                shadeTB.setTag(null);
                autoWindowSW.setTag(null);
                autoShadeSW.setTag(null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Server není dostupný", Toast.LENGTH_LONG).show();
            }
        });
        que.add(JSORequest);
    }

    private void uploadState(){
        // Create JSON content
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("light", String.valueOf(lightSW.isChecked()));
        params.put("brightness", String.valueOf(brightSB.getProgress()));
        params.put("autoLight", String.valueOf(autoLightSW.isChecked()));
        params.put("windowOpen", String.valueOf(windowTB.isChecked()));
        params.put("shadeOpen", String.valueOf(shadeTB.isChecked()));
        params.put("windowAuto", String.valueOf(autoWindowSW.isChecked()));
        params.put("shadeAuto", String.valueOf(autoShadeSW.isChecked()));

        //String URL = "http://192.168.0.213:54321/light";
        SharedPreferences shPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String URL = "http://" + shPref.getString("server", "192.168.0.213") + ":54321/light";


        RequestQueue que = Volley.newRequestQueue(getContext());
        que.start();

        JsonObjectRequest JSORequest = new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Server není dostupný", Toast.LENGTH_LONG).show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        que.add(JSORequest);
    }

    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(getContext(), "on resume light", Toast.LENGTH_SHORT).show();
        loadData();
    }

}