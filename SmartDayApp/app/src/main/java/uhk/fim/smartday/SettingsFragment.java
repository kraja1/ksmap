package uhk.fim.smartday;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {

    private EditText timeUpET;
    private EditText timeSleepET;
    private Button minusBTDay;
    private TextView dayTempTV;
    private Button plusBTDay;
    private Button minusBTNight;
    private TextView nightTempTV;
    private Button plusBTNight;
    private EditText cityET;
    private EditText serverET;
    private Button saveBT;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        timeUpET = (EditText) getView().findViewById(R.id.editTextTimeUp);
        timeSleepET = (EditText) getView().findViewById(R.id.editTextTimeSleep);
        minusBTDay = (Button) getView().findViewById(R.id.buttonMinus2);
        dayTempTV = (TextView) getView().findViewById(R.id.textViewDayTempVal);
        plusBTDay = (Button) getView().findViewById(R.id.buttonPlus2);
        minusBTNight = (Button) getView().findViewById(R.id.buttonMinus3);
        nightTempTV = (TextView) getView().findViewById(R.id.textViewNightTempVal);
        plusBTNight = (Button) getView().findViewById(R.id.buttonPlus3);
        cityET = (EditText) getView().findViewById(R.id.editTextCityName);
        serverET = (EditText) getView().findViewById(R.id.editTextServerName);
        saveBT = (Button) getView().findViewById(R.id.buttonSave);

        /* Load data from SmartDayCenter */
        loadSettings();

        plusBTDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double dayTemp = Double.parseDouble(dayTempTV.getText().toString().trim().replace(" °C", ""));
                dayTemp += 0.5;
                String temp = String.valueOf(dayTemp);
                dayTempTV.setText(temp + " °C");
            }
        });

        minusBTDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double dayTemp = Double.parseDouble(dayTempTV.getText().toString().trim().replace(" °C", ""));
                dayTemp -= 0.5;
                String temp = String.valueOf(dayTemp);
                dayTempTV.setText(temp + " °C");
            }
        });

        plusBTNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double nightTemp = Double.parseDouble(nightTempTV.getText().toString().trim().replace(" °C", ""));
                nightTemp += 0.5;
                String temp = String.valueOf(nightTemp);
                nightTempTV.setText(temp + " °C");
            }
        });

        minusBTNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double nightTemp = Double.parseDouble(nightTempTV.getText().toString().trim().replace(" °C", ""));
                nightTemp -= 0.5;
                String temp = String.valueOf(nightTemp);
                nightTempTV.setText(temp + " °C");
            }
        });

        saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
            }
        });
    }

    /* Download settings from SmartDayCenter */
    private void loadSettings() {
        SharedPreferences shPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        RequestQueue que = Volley.newRequestQueue(getContext());
        que.start();

        //String URL = "http://192.168.0.213:54321/settings";
        // Use saved server URL
        String URL = "http://" + shPref.getString("server", "192.168.0.213") + ":54321/settings";

        JsonObjectRequest JSORequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String timeUp="07:30";
                String timeSleep="22:30";
                double dayTemp=22;
                double nightTemp=16;
                String city="Praha";
                String server="localhost";

                try {
                    timeUp = response.getString("timeUp");
                    timeSleep = response.getString("timeSleep");
                    dayTemp = response.getDouble("dayTemp");
                    nightTemp = response.getDouble("nightTemp");
                    city = response.getString("city");
                    server = response.getString("server");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                timeUpET.setText(timeUp);
                timeSleepET.setText(timeSleep);
                dayTempTV.setText(String.valueOf(dayTemp) + " °C");
                nightTempTV.setText(String.valueOf(nightTemp) + " °C");
                cityET.setText(city);
                serverET.setText(server);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Server není dostupný", Toast.LENGTH_LONG).show();
            }
        });

        que.add(JSORequest);
    }

    /* Save settings to local preferences and upload to SmartDayCenter */
    private void saveSettings() {
        String timeUp = timeUpET.getText().toString();
        String timeSleep = timeSleepET.getText().toString();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
            try{
                formatter.parse(timeUp);
            }catch (DateTimeParseException e) {
                Toast.makeText(getContext(), "Neplatný formát času vstávání (HH:MM)", Toast.LENGTH_SHORT).show();
                return;
            }

            try{
                formatter.parse(timeSleep);
            }catch (DateTimeParseException e) {
                Toast.makeText(getContext(), "Neplatný formát času spánku (HH:MM)", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        SharedPreferences shPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shPref.edit();
        editor.putString("timeUp", timeUpET.getText().toString());
        editor.putString("timeSleep", timeSleepET.getText().toString());
        editor.putString("dayTemp", dayTempTV.getText().toString());
        editor.putString("nightTemp", nightTempTV.getText().toString());
        editor.putString("city", cityET.getText().toString());
        editor.putString("server", serverET.getText().toString());
        editor.apply();

        // Create JSON content
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("timeUp", timeUpET.getText().toString());
        params.put("timeSleep", timeSleepET.getText().toString());
        params.put("dayTemp", dayTempTV.getText().toString().trim().replace(" °C",""));
        params.put("nightTemp", nightTempTV.getText().toString().trim().replace(" °C",""));
        params.put("city", cityET.getText().toString());
        params.put("server", serverET.getText().toString());

        //String URL = "http://192.168.0.213:54321/settings";
        // Use saved server URL
        String URL = "http://" + shPref.getString("server", "192.168.0.213") + ":54321/settings";

        RequestQueue que = Volley.newRequestQueue(getContext());
        que.start();
        JsonObjectRequest JSORequest = new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Server není dostupný", Toast.LENGTH_LONG).show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        que.add(JSORequest);

        Toast.makeText(getContext(), "Nastavení uloženo", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(getContext(), "on resume settings", Toast.LENGTH_SHORT).show();
        loadSettings();
    }
}